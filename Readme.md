
## Запуск всего проекта

Вызов "go run ." из корневой папки выполнит все нижеизложенные команды: построит необходимы файлы для grpc, создаст/обновит swagger документацию, протестирует rest и grpc запросы, запустит rest и grpc сервера 

## Сборка проекта

Из корневой папки выполнить две команды

protoc --go_out=. --go_opt=paths=source_relative api/grpc/app/app.proto

protoc --go-grpc_out=. --go-grpc_opt=paths=source_relative api/grpc/app/app.proto

## Запуск отдельных срверов

Из корневой папки вызвать одну из команд:

1. go run smsService/api/grpc/server/.     
   
2. go run smsService/api/rest/server/.

## Тесты

Из корневой папки вызвать команду:

go test smsService/api/rest/test/.

## Swagger

После любых изменений в описании классов выполнить команду "swag init -o ./api/rest/server/docs"