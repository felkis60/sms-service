package startup

import (
	"log"

	db "gitlab.com/felkis60/sms-service/database"
	l "gitlab.com/felkis60/sms-service/log"

	"github.com/joho/godotenv"
)

func SystemStartup(loadEnv_ bool, logs_ bool) {

	if loadEnv_ {
		if !envInited {
			envInited = true
			err := godotenv.Load("configs/.env")
			if err != nil {
				log.Fatal(l.ERREnvLoad)
			}
		}
	}

	if logs_ {
		if !logsInited {
			logsInited = true
			l.StartLogs()
		}
	}

	if !basicInited {
		basicInited = true
		db.Start()
		db.Migrate()
	}

}

var basicInited = false
var envInited = false
var logsInited = false
