#!/bin/bash

docker stop sms_service-rest_stage || true
docker stop sms_service-grpc_stage || true
docker rm sms_service-rest_stage || true
docker rm sms_service-grpc_stage || true
docker build -f dockerfile.rest -t sms_service-rest_stage . --network=host
docker build -f dockerfile.grpc -t sms_service-grpc_stage . --network=host
docker run --network host -d -p 6330:6330 -v `pwd`:/srv/app --name sms_service-rest_stage sms_service-rest_stage
docker run --network host -d -p 6331:6331 -v `pwd`:/srv/app --name sms_service-grpc_stage sms_service-grpc_stage