#!/bin/bash

docker stop sms_service-rest || true
docker stop sms_service-grpc || true
docker rm sms_service-rest || true
docker rm sms_service-grpc || true
docker build -f dockerfile.rest -t sms_service-rest . --network=host
docker build -f dockerfile.grpc -t sms_service-grpc . --network=host
docker run --network host -d -p 6320:6320 -v `pwd`:/srv/app --name sms_service-rest sms_service-rest
docker run --network host -d -p 6321:6321 -v `pwd`:/srv/app --name sms_service-grpc sms_service-grpc