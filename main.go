package main

import (
	"log"
	"os/exec"
	"sync"
)

// @title SMS Service
// @version 0.1
// @description Microservice that sends SMS.

func main() {

	var wg sync.WaitGroup
	cmd1 := exec.Command("protoc", "--go_out=.", "--go_opt=paths=source_relative", "api/grpc/app/app.proto")
	cmd2 := exec.Command("protoc", "--go-grpc_out=.", "--go-grpc_opt=paths=source_relative", "api/grpc/app/app.proto")
	cmd3 := exec.Command("swag", "init", "-o", "./api/rest/server/docs")
	cmd4 := exec.Command("go", "test", "smsService/api/rest/test/.")
	cmd5 := exec.Command("go", "test", "smsService/api/grpc/test/.")
	cmd6 := exec.Command("go", "run", "smsService/api/grpc/server/.")
	cmd7 := exec.Command("go", "run", "smsService/api/rest/server/.")

	if _, err := cmd1.Output(); err != nil {
		log.Fatalf(err.Error())
	}

	if _, err := cmd2.Output(); err != nil {
		log.Fatalf(err.Error())
	}

	if _, err := cmd3.Output(); err != nil {
		log.Fatalf(err.Error())
	}

	if _, err := cmd4.Output(); err != nil {
		log.Fatalf(err.Error())
	}

	if _, err := cmd5.Output(); err != nil {
		log.Fatalf(err.Error())
	}

	wg.Add(1)
	go cmd6.Output()
	wg.Add(1)
	go cmd7.Output()

	wg.Wait()

}
