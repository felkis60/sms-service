package sms

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"

	l "gitlab.com/felkis60/sms-service/log"
	t "gitlab.com/felkis60/sms-service/models"
)

func SendVSMS(wallet_ *t.Wallet, phoneNumber_ string, from_ string, text_ string) ([]byte, error) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "SendSMS")
	}

	var params = url.Values{}

	params.Set("login", wallet_.SMSCLogin)
	params.Set("psw", wallet_.SMSCPass)
	params.Set("phones", phoneNumber_)
	params.Set("mes", text_)
	params.Set("sender", from_)
	params.Set("fmt", "3")

	resp, err := http.Get("https://smsc.ru/sys/send.php?" + params.Encode())
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var objmap t.SMSCResponce
	err = json.Unmarshal(respBody, &objmap)
	if err != nil {
		log.Print(err)
		return nil, err
	}

	if objmap.Error != nil {
		if objmap.ErrorCode != nil {
			return nil, errors.New("sms wasn't sent: " + *objmap.Error + "(" + strconv.FormatFloat(*objmap.ErrorCode, 'f', -1, 64) + ")")
		}
		return nil, errors.New("sms wasn't sent: " + *objmap.Error)
	}

	if objmap.Cnt != nil {
		if *objmap.Cnt == "0" {
			return respBody, errors.New("sms was not sent")
		}
	}

	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "SendSMS")
	}

	return respBody, nil
}

func SendSMSC(wallet_ *t.Wallet, phoneNumbers_ []string, from_ string, text_ string) (interface{}, error) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "SendSMS")
	}

	var AllPhonesString string

	for i := range phoneNumbers_ {
		if i > 0 {
			AllPhonesString += ";"
		}
		AllPhonesString += phoneNumbers_[i]
	}

	var params = url.Values{}
	params.Set("login", wallet_.Nickname)
	params.Set("psw", wallet_.Key)
	params.Set("phones", AllPhonesString)
	params.Set("mes", text_)

	resp, err := http.Get("https://smsc.ru/sys/send.php?" + params.Encode())
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var data interface{}
	err = json.Unmarshal(respBody, &data)
	if err != nil {
		return nil, err
	}

	return data, nil
}
