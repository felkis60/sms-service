package other

import (
	"context"
	"log"

	pb "gitlab.com/felkis60/sms-service/api/grpc/app"
	v "gitlab.com/felkis60/sms-service/controllers"
	l "gitlab.com/felkis60/sms-service/log"
	t "gitlab.com/felkis60/sms-service/models"
)

type Server struct {
	pb.UnimplementedSMSserviceServer
}

func (s *Server) RequestCode(ctx context.Context, in *pb.SMSRequest) (*pb.ServiceResponce, error) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "RequestCode")
	}
	input := t.InputRegistration{PhoneNumber: in.GetPhoneNumber(), Token: in.GetWalletToken()}
	responce := v.CreateAndSendVerificationMessege(&input)
	tempStatus := false
	if responce.Code == 200 {
		tempStatus = true
	} else {
		tempStatus = false
	}
	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "RequestCode")
	}
	return &pb.ServiceResponce{Status: tempStatus, Code: int64(responce.Code), Message: responce.Message}, nil

}

func (s *Server) VerifyCode(ctx context.Context, in *pb.SMSVerify) (*pb.ServiceResponce, error) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "VerifyCode")
	}
	input := t.InputVerify{PhoneNumber: in.GetPhoneNumber(), Code: in.GetAuthCode()}
	responce := v.VerificateCode(&input)
	tempStatus := false
	if responce.Code == 200 {
		tempStatus = true
	} else {
		tempStatus = false
	}
	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "VerifyCode")
	}
	return &pb.ServiceResponce{Status: tempStatus, Code: int64(responce.Code), Message: responce.Message}, nil
}

func (s *Server) SendSMS(ctx context.Context, in *pb.SMSSend) (*pb.ServiceResponce, error) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "SendSMS")
	}
	input := t.InputSendSMS{From: in.From, PhoneNumber: in.PhoneNumber, Token: in.WalletToken, Text: in.Text}
	responce, err := v.SendTextSMS(&input)
	tempStatus := false
	if responce.Code == 200 && err == nil {
		tempStatus = true
	} else {
		tempStatus = false
	}
	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "SendSMS")
	}
	return &pb.ServiceResponce{Status: tempStatus, Code: int64(responce.Code), Message: responce.Message}, err
}
