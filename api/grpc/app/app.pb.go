// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.18.0--rc1
// source: api/grpc/app/app.proto

package app

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type SMSRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PhoneNumber string `protobuf:"bytes,1,opt,name=PhoneNumber,proto3" json:"PhoneNumber,omitempty"`
	WalletToken string `protobuf:"bytes,2,opt,name=WalletToken,proto3" json:"WalletToken,omitempty"`
	From        string `protobuf:"bytes,3,opt,name=From,proto3" json:"From,omitempty"`
}

func (x *SMSRequest) Reset() {
	*x = SMSRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_grpc_app_app_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SMSRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SMSRequest) ProtoMessage() {}

func (x *SMSRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_grpc_app_app_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SMSRequest.ProtoReflect.Descriptor instead.
func (*SMSRequest) Descriptor() ([]byte, []int) {
	return file_api_grpc_app_app_proto_rawDescGZIP(), []int{0}
}

func (x *SMSRequest) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *SMSRequest) GetWalletToken() string {
	if x != nil {
		return x.WalletToken
	}
	return ""
}

func (x *SMSRequest) GetFrom() string {
	if x != nil {
		return x.From
	}
	return ""
}

type SMSVerify struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PhoneNumber string `protobuf:"bytes,1,opt,name=PhoneNumber,proto3" json:"PhoneNumber,omitempty"`
	AuthCode    string `protobuf:"bytes,2,opt,name=AuthCode,proto3" json:"AuthCode,omitempty"`
}

func (x *SMSVerify) Reset() {
	*x = SMSVerify{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_grpc_app_app_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SMSVerify) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SMSVerify) ProtoMessage() {}

func (x *SMSVerify) ProtoReflect() protoreflect.Message {
	mi := &file_api_grpc_app_app_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SMSVerify.ProtoReflect.Descriptor instead.
func (*SMSVerify) Descriptor() ([]byte, []int) {
	return file_api_grpc_app_app_proto_rawDescGZIP(), []int{1}
}

func (x *SMSVerify) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *SMSVerify) GetAuthCode() string {
	if x != nil {
		return x.AuthCode
	}
	return ""
}

type SMSSend struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	PhoneNumber string `protobuf:"bytes,1,opt,name=PhoneNumber,proto3" json:"PhoneNumber,omitempty"`
	WalletToken string `protobuf:"bytes,2,opt,name=WalletToken,proto3" json:"WalletToken,omitempty"`
	Text        string `protobuf:"bytes,3,opt,name=Text,proto3" json:"Text,omitempty"`
	From        string `protobuf:"bytes,4,opt,name=From,proto3" json:"From,omitempty"`
}

func (x *SMSSend) Reset() {
	*x = SMSSend{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_grpc_app_app_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SMSSend) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SMSSend) ProtoMessage() {}

func (x *SMSSend) ProtoReflect() protoreflect.Message {
	mi := &file_api_grpc_app_app_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SMSSend.ProtoReflect.Descriptor instead.
func (*SMSSend) Descriptor() ([]byte, []int) {
	return file_api_grpc_app_app_proto_rawDescGZIP(), []int{2}
}

func (x *SMSSend) GetPhoneNumber() string {
	if x != nil {
		return x.PhoneNumber
	}
	return ""
}

func (x *SMSSend) GetWalletToken() string {
	if x != nil {
		return x.WalletToken
	}
	return ""
}

func (x *SMSSend) GetText() string {
	if x != nil {
		return x.Text
	}
	return ""
}

func (x *SMSSend) GetFrom() string {
	if x != nil {
		return x.From
	}
	return ""
}

type ServiceResponce struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Status  bool   `protobuf:"varint,1,opt,name=Status,proto3" json:"Status,omitempty"`
	Code    int64  `protobuf:"varint,2,opt,name=Code,proto3" json:"Code,omitempty"`
	Message string `protobuf:"bytes,3,opt,name=Message,proto3" json:"Message,omitempty"`
}

func (x *ServiceResponce) Reset() {
	*x = ServiceResponce{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_grpc_app_app_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ServiceResponce) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ServiceResponce) ProtoMessage() {}

func (x *ServiceResponce) ProtoReflect() protoreflect.Message {
	mi := &file_api_grpc_app_app_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ServiceResponce.ProtoReflect.Descriptor instead.
func (*ServiceResponce) Descriptor() ([]byte, []int) {
	return file_api_grpc_app_app_proto_rawDescGZIP(), []int{3}
}

func (x *ServiceResponce) GetStatus() bool {
	if x != nil {
		return x.Status
	}
	return false
}

func (x *ServiceResponce) GetCode() int64 {
	if x != nil {
		return x.Code
	}
	return 0
}

func (x *ServiceResponce) GetMessage() string {
	if x != nil {
		return x.Message
	}
	return ""
}

var File_api_grpc_app_app_proto protoreflect.FileDescriptor

var file_api_grpc_app_app_proto_rawDesc = []byte{
	0x0a, 0x16, 0x61, 0x70, 0x69, 0x2f, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x61, 0x70, 0x70, 0x2f, 0x61,
	0x70, 0x70, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x03, 0x61, 0x70, 0x70, 0x22, 0x64, 0x0a,
	0x0a, 0x53, 0x4d, 0x53, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12, 0x20, 0x0a, 0x0b, 0x50,
	0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0b, 0x50, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x20, 0x0a,
	0x0b, 0x57, 0x61, 0x6c, 0x6c, 0x65, 0x74, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x02, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x0b, 0x57, 0x61, 0x6c, 0x6c, 0x65, 0x74, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x12,
	0x12, 0x0a, 0x04, 0x46, 0x72, 0x6f, 0x6d, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x46,
	0x72, 0x6f, 0x6d, 0x22, 0x49, 0x0a, 0x09, 0x53, 0x4d, 0x53, 0x56, 0x65, 0x72, 0x69, 0x66, 0x79,
	0x12, 0x20, 0x0a, 0x0b, 0x50, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b, 0x50, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62,
	0x65, 0x72, 0x12, 0x1a, 0x0a, 0x08, 0x41, 0x75, 0x74, 0x68, 0x43, 0x6f, 0x64, 0x65, 0x18, 0x02,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x08, 0x41, 0x75, 0x74, 0x68, 0x43, 0x6f, 0x64, 0x65, 0x22, 0x75,
	0x0a, 0x07, 0x53, 0x4d, 0x53, 0x53, 0x65, 0x6e, 0x64, 0x12, 0x20, 0x0a, 0x0b, 0x50, 0x68, 0x6f,
	0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0b,
	0x50, 0x68, 0x6f, 0x6e, 0x65, 0x4e, 0x75, 0x6d, 0x62, 0x65, 0x72, 0x12, 0x20, 0x0a, 0x0b, 0x57,
	0x61, 0x6c, 0x6c, 0x65, 0x74, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09,
	0x52, 0x0b, 0x57, 0x61, 0x6c, 0x6c, 0x65, 0x74, 0x54, 0x6f, 0x6b, 0x65, 0x6e, 0x12, 0x12, 0x0a,
	0x04, 0x54, 0x65, 0x78, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x54, 0x65, 0x78,
	0x74, 0x12, 0x12, 0x0a, 0x04, 0x46, 0x72, 0x6f, 0x6d, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x04, 0x46, 0x72, 0x6f, 0x6d, 0x22, 0x57, 0x0a, 0x0f, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65,
	0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x63, 0x65, 0x12, 0x16, 0x0a, 0x06, 0x53, 0x74, 0x61, 0x74,
	0x75, 0x73, 0x18, 0x01, 0x20, 0x01, 0x28, 0x08, 0x52, 0x06, 0x53, 0x74, 0x61, 0x74, 0x75, 0x73,
	0x12, 0x12, 0x0a, 0x04, 0x43, 0x6f, 0x64, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04,
	0x43, 0x6f, 0x64, 0x65, 0x12, 0x18, 0x0a, 0x07, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x4d, 0x65, 0x73, 0x73, 0x61, 0x67, 0x65, 0x32, 0xab,
	0x01, 0x0a, 0x0a, 0x53, 0x4d, 0x53, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x36, 0x0a,
	0x0b, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x43, 0x6f, 0x64, 0x65, 0x12, 0x0f, 0x2e, 0x61,
	0x70, 0x70, 0x2e, 0x53, 0x4d, 0x53, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x14, 0x2e,
	0x61, 0x70, 0x70, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x63, 0x65, 0x22, 0x00, 0x12, 0x34, 0x0a, 0x0a, 0x56, 0x65, 0x72, 0x69, 0x66, 0x79, 0x43,
	0x6f, 0x64, 0x65, 0x12, 0x0e, 0x2e, 0x61, 0x70, 0x70, 0x2e, 0x53, 0x4d, 0x53, 0x56, 0x65, 0x72,
	0x69, 0x66, 0x79, 0x1a, 0x14, 0x2e, 0x61, 0x70, 0x70, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x63, 0x65, 0x22, 0x00, 0x12, 0x2f, 0x0a, 0x07, 0x53,
	0x65, 0x6e, 0x64, 0x53, 0x4d, 0x53, 0x12, 0x0c, 0x2e, 0x61, 0x70, 0x70, 0x2e, 0x53, 0x4d, 0x53,
	0x53, 0x65, 0x6e, 0x64, 0x1a, 0x14, 0x2e, 0x61, 0x70, 0x70, 0x2e, 0x53, 0x65, 0x72, 0x76, 0x69,
	0x63, 0x65, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x63, 0x65, 0x22, 0x00, 0x42, 0x1b, 0x5a, 0x19,
	0x73, 0x6d, 0x73, 0x67, 0x6f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2f, 0x61, 0x70, 0x69,
	0x2f, 0x67, 0x72, 0x70, 0x63, 0x2f, 0x61, 0x70, 0x70, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x33,
}

var (
	file_api_grpc_app_app_proto_rawDescOnce sync.Once
	file_api_grpc_app_app_proto_rawDescData = file_api_grpc_app_app_proto_rawDesc
)

func file_api_grpc_app_app_proto_rawDescGZIP() []byte {
	file_api_grpc_app_app_proto_rawDescOnce.Do(func() {
		file_api_grpc_app_app_proto_rawDescData = protoimpl.X.CompressGZIP(file_api_grpc_app_app_proto_rawDescData)
	})
	return file_api_grpc_app_app_proto_rawDescData
}

var file_api_grpc_app_app_proto_msgTypes = make([]protoimpl.MessageInfo, 4)
var file_api_grpc_app_app_proto_goTypes = []interface{}{
	(*SMSRequest)(nil),      // 0: app.SMSRequest
	(*SMSVerify)(nil),       // 1: app.SMSVerify
	(*SMSSend)(nil),         // 2: app.SMSSend
	(*ServiceResponce)(nil), // 3: app.ServiceResponce
}
var file_api_grpc_app_app_proto_depIdxs = []int32{
	0, // 0: app.SMSservice.RequestCode:input_type -> app.SMSRequest
	1, // 1: app.SMSservice.VerifyCode:input_type -> app.SMSVerify
	2, // 2: app.SMSservice.SendSMS:input_type -> app.SMSSend
	3, // 3: app.SMSservice.RequestCode:output_type -> app.ServiceResponce
	3, // 4: app.SMSservice.VerifyCode:output_type -> app.ServiceResponce
	3, // 5: app.SMSservice.SendSMS:output_type -> app.ServiceResponce
	3, // [3:6] is the sub-list for method output_type
	0, // [0:3] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_api_grpc_app_app_proto_init() }
func file_api_grpc_app_app_proto_init() {
	if File_api_grpc_app_app_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_api_grpc_app_app_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SMSRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_grpc_app_app_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SMSVerify); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_grpc_app_app_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SMSSend); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_grpc_app_app_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ServiceResponce); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_api_grpc_app_app_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   4,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_api_grpc_app_app_proto_goTypes,
		DependencyIndexes: file_api_grpc_app_app_proto_depIdxs,
		MessageInfos:      file_api_grpc_app_app_proto_msgTypes,
	}.Build()
	File_api_grpc_app_app_proto = out.File
	file_api_grpc_app_app_proto_rawDesc = nil
	file_api_grpc_app_app_proto_goTypes = nil
	file_api_grpc_app_app_proto_depIdxs = nil
}
