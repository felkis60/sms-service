package main

import (
	"context"
	"log"
	"time"

	pb "gitlab.com/felkis60/sms-service/api/grpc/app"

	"google.golang.org/grpc"
)

func main() {
	// Set up a connection to the server.

	conn, err := grpc.Dial("localhost:50051", grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewSMSserviceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.SendSMS(ctx, &pb.SMSSend{PhoneNumber: "7909", WalletToken: "", Text: "Hi from grpc!"})
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
	log.Println(r.GetMessage())

	//name := "Gladys"
	//want := regexp.MustCompile(`\b` + name + `\b`)
	//msg, err := Hello("Gladys")
	//if !want.MatchString(msg) || err != nil {
	//	t.Fatalf(`Hello("Gladys") = %q, %v, want match for %#q, nil`, msg, err, want)
	//}

}
