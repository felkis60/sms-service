package main

import (
	"context"
	"net"
	"os"
	"testing"
	"time"

	pb "gitlab.com/felkis60/sms-service/api/grpc/app"
	other "gitlab.com/felkis60/sms-service/api/grpc/other"
	db "gitlab.com/felkis60/sms-service/database"
	logs "gitlab.com/felkis60/sms-service/log"
	models "gitlab.com/felkis60/sms-service/models"
	start "gitlab.com/felkis60/sms-service/startup"
	helper "gitlab.com/felkis60/sms-service/tools"

	"github.com/go-playground/assert/v2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/test/bufconn"
)

func TestNoWalletTokenTest(t *testing.T) {

	ctx := context.Background()

	client, closer := newServer(ctx)
	defer closer()

	resp, _ := client.RequestCode(ctx, &pb.SMSRequest{PhoneNumber: "79999999999", WalletToken: ""})
	assert.Equal(t, int64(400), resp.Code)
	assert.Equal(t, false, resp.Status)
	assert.Equal(t, logs.ERRTokenRequested, resp.Message)

	resp, _ = client.VerifyCode(ctx, &pb.SMSVerify{PhoneNumber: "32123", AuthCode: "1234"})
	assert.Equal(t, int64(400), resp.Code)
	assert.Equal(t, false, resp.Status)
	assert.Equal(t, logs.ERRNoSuchPhNum, resp.Message)

	//resp, _ = client.SendSMS(ctx, &pb.SMSSend{PhoneNumber: "79999999999", WalletToken: "", Text: "text", From: "from"})
	//assert.Equal(t, int64(400), resp.Code)
	//assert.Equal(t, false, resp.Status)
	//assert.Equal(t, logs.ERRTokenRequested, resp.Message)

}

func TestWrongWalletToken(t *testing.T) {

	ctx := context.Background()

	client, closer := newServer(ctx)
	defer closer()

	resp, _ := client.RequestCode(ctx, &pb.SMSRequest{PhoneNumber: "79999999999", WalletToken: "test"})
	assert.Equal(t, int64(400), resp.Code)
	assert.Equal(t, false, resp.Status)
	assert.Equal(t, logs.ERRNoSuchWalToken, resp.Message)

	//resp, _ = client.SendSMS(ctx, &pb.SMSSend{PhoneNumber: "79999999999", WalletToken: "test", Text: "text", From: "from"})
	//assert.Equal(t, int64(400), resp.Code)
	//assert.Equal(t, false, resp.Status)
	//assert.Equal(t, logs.ERRNoSuchWalToken, resp.Message)

}

func TestNoPhoneNumber(t *testing.T) {

	ctx := context.Background()

	client, closer := newServer(ctx)
	defer closer()

	resp, _ := client.RequestCode(ctx, &pb.SMSRequest{PhoneNumber: "", WalletToken: os.Getenv("TRUETOKEN")})
	assert.Equal(t, int64(400), resp.Code)
	assert.Equal(t, false, resp.Status)
	assert.Equal(t, logs.ERRPhNumRequested, resp.Message)

	resp, _ = client.VerifyCode(ctx, &pb.SMSVerify{PhoneNumber: "", AuthCode: "1234"})
	assert.Equal(t, int64(400), resp.Code)
	assert.Equal(t, false, resp.Status)
	assert.Equal(t, logs.ERRPhNumRequested, resp.Message)

	//resp, _ = client.SendSMS(ctx, &pb.SMSSend{PhoneNumber: "", WalletToken: os.Getenv("TRUETOKEN"), Text: "text", From: "from"})
	//assert.Equal(t, int64(400), resp.Code)
	//assert.Equal(t, false, resp.Status)
	//assert.Equal(t, logs.ERRPhNumRequested, resp.Message)

}

func TestExpiredCode(t *testing.T) {

	ctx := context.Background()

	client, closer := newServer(ctx)
	defer closer()

	db.Db.Create(&models.VerificationCode{PhoneNumber: "999999", AuthCode: helper.GenerateMD5("1234"), ExpiredAt: time.Now().Add(time.Hour * -1)})

	resp, _ := client.VerifyCode(ctx, &pb.SMSVerify{PhoneNumber: "999999", AuthCode: "1234"})
	assert.Equal(t, int64(400), resp.Code)
	assert.Equal(t, false, resp.Status)
	assert.Equal(t, logs.ERRCodeExpired, resp.Message)
}

func TestWrongCode(t *testing.T) {

	ctx := context.Background()

	client, closer := newServer(ctx)
	defer closer()

	db.Db.Create(&models.VerificationCode{PhoneNumber: "999", AuthCode: helper.GenerateMD5("1234"), ExpiredAt: time.Now().Add(time.Hour)})

	resp, _ := client.VerifyCode(ctx, &pb.SMSVerify{PhoneNumber: "999", AuthCode: "1235"})
	assert.Equal(t, int64(400), resp.Code)
	assert.Equal(t, false, resp.Status)
	assert.Equal(t, logs.ERRWrongCode, resp.Message)
}

func TestRightCode(t *testing.T) {

	ctx := context.Background()

	client, closer := newServer(ctx)
	defer closer()

	db.Db.Create(&models.VerificationCode{PhoneNumber: "9999", AuthCode: helper.GenerateMD5("1234"), ExpiredAt: time.Now().Add(time.Hour)})

	resp, _ := client.VerifyCode(ctx, &pb.SMSVerify{PhoneNumber: "9999", AuthCode: "1234"})
	assert.Equal(t, int64(200), resp.Code)
	assert.Equal(t, true, resp.Status)
	assert.Equal(t, "Success!", resp.Message)

}

func TestMain(m *testing.M) {

	os.Setenv("HOST", "localhost")
	os.Setenv("USER", "postgres")
	os.Setenv("PASSWORD", "admin")
	os.Setenv("DBNAME", "numbertest")
	os.Setenv("DBPORT", "5432")
	os.Setenv("SSLMODE", "disable")
	os.Setenv("TIMEZONE", "3")
	os.Setenv("INFOLOG", "false")
	os.Setenv("TRUETOKEN", "1234567890123456")
	defer os.Clearenv()

	start.SystemStartup(false, false)

	code := m.Run()

	os.Exit(code)
}

func newServer(ctx context.Context) (pb.SMSserviceClient, func()) {
	buffer := 1024 * 1024
	listener := bufconn.Listen(buffer)

	s := grpc.NewServer()
	pb.RegisterSMSserviceServer(s, &other.Server{})
	go func() {
		if err := s.Serve(listener); err != nil {
			panic(err)
		}
	}()

	conn, _ := grpc.DialContext(ctx, "", grpc.WithContextDialer(func(context.Context, string) (net.Conn, error) {
		return listener.Dial()
	}), grpc.WithInsecure())

	closer := func() {
		listener.Close()
		s.Stop()
	}

	client := pb.NewSMSserviceClient(conn)

	return client, closer
}
