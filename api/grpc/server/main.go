package main

import (
	"log"
	"net"
	"os"

	pb "gitlab.com/felkis60/sms-service/api/grpc/app"
	other "gitlab.com/felkis60/sms-service/api/grpc/other"
	l "gitlab.com/felkis60/sms-service/log"
	start "gitlab.com/felkis60/sms-service/startup"

	"google.golang.org/grpc"
)

func main() {

	start.SystemStartup(true, true)

	lis, err := net.Listen("tcp", ":"+os.Getenv("GRPC_PORT"))
	if err != nil {
		log.Fatalf(l.ERRListen, err)
	}

	s := grpc.NewServer()
	pb.RegisterSMSserviceServer(s, &other.Server{})

	log.Printf(l.INFStartServer, "grcp")

	if err := s.Serve(lis); err != nil {
		log.Fatalf(l.ERRServe, err)
	}

}
