package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"

	setup "gitlab.com/felkis60/sms-service/api/rest/server/other"
	db "gitlab.com/felkis60/sms-service/database"
	logs "gitlab.com/felkis60/sms-service/log"
	models "gitlab.com/felkis60/sms-service/models"
	start "gitlab.com/felkis60/sms-service/startup"
	helper "gitlab.com/felkis60/sms-service/tools"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/assert/v2"
)

var router *gin.Engine

func TestNoInput(t *testing.T) {

	w := httptest.NewRecorder()
	var resp map[string]string

	req, _ := http.NewRequest("POST", "/registration", nil)
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRInputNilPointer, "ERROR: "+resp["error"])

	req, _ = http.NewRequest("POST", "/sendsms", nil)
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRInputNilPointer, "ERROR: "+resp["error"])

	req, _ = http.NewRequest("POST", "/verify", nil)
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRInputNilPointer, "ERROR: "+resp["error"])

	req, _ = http.NewRequest("POST", "/smslogs", nil)
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRInputNilPointer, "ERROR: "+resp["error"])

}

func TestNoWalletToken(t *testing.T) {

	w := httptest.NewRecorder()
	var resp map[string]string

	body, _ := json.Marshal(&models.InputRegistration{PhoneNumber: "79099159376", Token: ""})
	req, _ := http.NewRequest("POST", "/registration", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRTokenRequested, resp["error"])

	body, _ = json.Marshal(&models.InputSendSMS{PhoneNumber: "79099159376", Token: ""})
	req, _ = http.NewRequest("POST", "/sendsms", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRTokenRequested, resp["error"])

	body, _ = json.Marshal(&models.InputFieldsLogSearch{Token: "", From: "", Search: "", To: "", Start: "", End: "", SortBy: ""})
	req, _ = http.NewRequest("POST", "/smslogs", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRTokenRequested, resp["error"])

}

func TestWrongWalletToken(t *testing.T) {

	w := httptest.NewRecorder()
	var resp map[string]string

	body, _ := json.Marshal(&models.InputRegistration{PhoneNumber: "79099159376", Token: "123"})
	req, _ := http.NewRequest("POST", "/registration", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRNoSuchWalToken, resp["error"])

	body, _ = json.Marshal(&models.InputSendSMS{PhoneNumber: "79099159376", Token: "123"})
	req, _ = http.NewRequest("POST", "/sendsms", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRNoSuchWalToken, resp["error"])

	body, _ = json.Marshal(&models.InputFieldsLogSearch{Token: "123", From: "", Search: "", To: "", Start: "", End: "", SortBy: ""})
	req, _ = http.NewRequest("POST", "/smslogs", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRNoSuchWalToken, resp["error"])

}

func TestNoPhoneNumber(t *testing.T) {

	w := httptest.NewRecorder()
	var resp map[string]string

	body, _ := json.Marshal(&models.InputRegistration{PhoneNumber: "", Token: os.Getenv("TRUETOKEN")})
	req, _ := http.NewRequest("POST", "/registration", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRPhNumRequested, resp["error"])

	body, _ = json.Marshal(&models.InputSendSMS{PhoneNumber: "", Token: os.Getenv("TRUETOKEN")})
	req, _ = http.NewRequest("POST", "/sendsms", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRPhNumRequested, resp["error"])

	body, _ = json.Marshal(&models.InputVerify{PhoneNumber: "", Code: "4253"})
	req, _ = http.NewRequest("POST", "/verify", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRPhNumRequested, resp["error"])

}

func TestExpiredCode(t *testing.T) {

	w := httptest.NewRecorder()
	var resp map[string]string
	db.Db.Create(&models.VerificationCode{PhoneNumber: "999999", AuthCode: helper.GenerateMD5("1234"), ExpiredAt: time.Now().Add(time.Hour * -1)})

	body, _ := json.Marshal(&models.InputVerify{PhoneNumber: "999999", Code: "1234"})
	req, _ := http.NewRequest("POST", "/verify", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRCodeExpired, resp["error"])
}

func TestWrongCode(t *testing.T) {

	w := httptest.NewRecorder()
	var resp map[string]string
	db.Db.Create(&models.VerificationCode{PhoneNumber: "999", AuthCode: helper.GenerateMD5("1234"), ExpiredAt: time.Now().Add(time.Hour)})

	body, _ := json.Marshal(&models.InputVerify{PhoneNumber: "999", Code: "1235"})
	req, _ := http.NewRequest("POST", "/verify", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 400, w.Code)
	assert.Equal(t, logs.ERRWrongCode, resp["error"])
}

func TestRightCode(t *testing.T) {

	w := httptest.NewRecorder()
	var resp map[string]string
	db.Db.Create(&models.VerificationCode{PhoneNumber: "9999", AuthCode: helper.GenerateMD5("1234"), ExpiredAt: time.Now().Add(time.Hour)})

	body, _ := json.Marshal(&models.InputVerify{PhoneNumber: "9999", Code: "1234"})
	req, _ := http.NewRequest("POST", "/verify", bytes.NewReader(body))
	router.ServeHTTP(w, req)
	json.Unmarshal(w.Body.Bytes(), &resp)
	assert.Equal(t, 200, w.Code)
	assert.Equal(t, "Success!", resp["message"])
}

func TestMain(m *testing.M) {

	os.Setenv("HOST", "localhost")
	os.Setenv("USER", "postgres")
	os.Setenv("PASSWORD", "admin")
	os.Setenv("DBNAME", "numbertest")
	os.Setenv("DBPORT", "5432")
	os.Setenv("SSLMODE", "disable")
	os.Setenv("TIMEZONE", "3")
	os.Setenv("INFOLOG", "false")
	os.Setenv("TRUETOKEN", "1234567890123456")
	defer os.Clearenv()

	start.SystemStartup(false, false)
	router = setup.SetupRouter()

	code := m.Run()

	os.Exit(code)
}
