package other

import (
	"log"

	v "gitlab.com/felkis60/sms-service/controllers"
	l "gitlab.com/felkis60/sms-service/log"
	t "gitlab.com/felkis60/sms-service/models"

	"github.com/gin-gonic/gin"
)

//
// @Summary Sends auth code to the phone number
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ 				body t.InputRegistration true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string "error: 'token' is required, 'phone_number' is required, No such wallet token, Too fast!, The SMS was not sent!"
// @Router /registration [post]
func registrationRESTWrapper(c *gin.Context) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "registrationRESTWarper")
	}

	//Телефон для которого проверяем код
	var inputBody t.InputRegistration
	if err := c.ShouldBindJSON(&inputBody); err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	var responce = v.CreateAndSendVerificationMessege(&inputBody)

	if responce.Code == 200 {
		c.JSON(responce.Code, gin.H{"message": responce.Message})
	} else {
		c.JSON(responce.Code, gin.H{"error": responce.Message})
	}
	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "registrationRESTWarper")
	}
}

//
// @Summary Tries to auth phone
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ 				body t.InputVerify true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string "error: 'code' is required, 'phone_number' is required, No such phone number, Wrong code, Code is expired"
// @Router /verify [post]
func verificationRESTWrapper(c *gin.Context) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "verificationRESTWarper")
	}
	var inputBody t.InputVerify
	if err := c.ShouldBindJSON(&inputBody); err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	var responce = v.VerificateCode(&inputBody)

	if responce.Code == 200 {
		c.JSON(responce.Code, gin.H{"message": responce.Message})
	} else {
		c.JSON(responce.Code, gin.H{"error": responce.Message})
	}
	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "verificationRESTWarper")
	}

}

//
// @Summary Sends SMS to the phone number with specific text
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ 				body t.InputSendSMS true "Request body"
// @Success 200 {object} string "Success!"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string "error: 'token' is required, 'text' is required, 'phone_number' is required, No such wallet token, The SMS was not sent!"
// @Router /sendsms [post]
func sendSMSRESTWrapper(c *gin.Context) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "sendSMSRESTWarper")
	}
	var inputBody t.InputSendSMS
	if err := c.ShouldBindJSON(&inputBody); err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	responce, err := v.SendTextSMS(&inputBody)

	if responce.Code == 200 && err == nil {
		c.JSON(responce.Code, gin.H{"message": responce.Message})
	} else {
		c.JSON(responce.Code, gin.H{"error": err.Error()})
	}

	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "sendSMSRESTWarper")
	}
}

//
// @Summary Search SMS logs with parameters
// @Description
// @Accept  json
// @Produce  json
// @Param 	_ 				body t.InputFieldsLogSearch true "Request body"
// @Success 200 {object} string "json: items, total_pages, total_items"
// @Failure 500 {object} string "error: invalid request"
// @Failure 400 {object} string "error: 'token' is required, No such wallet token"
// @Router /smslogs [post]
func getSMSLogsWrapper(c *gin.Context) {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "getSMSLogsWarper")
	}

	var inputBody t.InputFieldsLogSearch
	if err := c.ShouldBindJSON(&inputBody); err != nil {
		c.JSON(400, gin.H{"error": err.Error()})
		return
	}

	code, responce, err := v.GetSMSLogs(&inputBody)

	if code == 200 && err == nil && responce != nil {
		c.JSON(code, *responce)
	} else {
		c.JSON(code, gin.H{"error": err.Error()})
	}

	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "getSMSLogsWarper")
	}
}
