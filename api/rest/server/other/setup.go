package other

import "github.com/gin-gonic/gin"

func SetupRouter() *gin.Engine {
	r := gin.Default()
	r.POST("/verify", verificationRESTWrapper)
	r.POST("/registration", registrationRESTWrapper)
	r.POST("/sendsms", sendSMSRESTWrapper)
	r.POST("/smslogs", getSMSLogsWrapper)
	return r
}
