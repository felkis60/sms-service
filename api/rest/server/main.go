package main

import (
	"log"
	"os"

	setup "gitlab.com/felkis60/sms-service/api/rest/server/other"
	l "gitlab.com/felkis60/sms-service/log"
	start "gitlab.com/felkis60/sms-service/startup"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	_ "gitlab.com/felkis60/sms-service/api/rest/server/docs"
)

func main() {
	start.SystemStartup(true, true)
	r := setup.SetupRouter()
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	log.Printf(l.INFStartServer, "rest")
	r.Run(":" + os.Getenv("RESTPORT"))

}
