package verificator

import (
	"errors"
	"fmt"
	"log"
	"math"
	"time"

	db "gitlab.com/felkis60/sms-service/database"
	s "gitlab.com/felkis60/sms-service/external"
	l "gitlab.com/felkis60/sms-service/log"
	t "gitlab.com/felkis60/sms-service/models"
	h "gitlab.com/felkis60/sms-service/tools"

	"gorm.io/datatypes"
)

// Функция для создания и отправки проверочного кода
func CreateAndSendVerificationMessege(input_ *t.InputRegistration) t.SimpleResponce {

	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "CreateAndSendVerificationMessege")
	}

	if input_ == nil {
		log.Print(l.ERRInputNilPointer)
		return t.SimpleResponce{Code: 500, Message: l.ERRInputNilPointer}
	}
	//Переменная в которую будем помещать найденный кошелек
	var dbWalletData t.Wallet
	//Переменная в которую будем помещать найденный телефон
	var dbPhoneData t.RegisteredPhoneNumber

	//Проверка запроса на наличие поля token в теле
	if input_.Token == "" {
		log.Print(l.ERRTokenRequested)
		return t.SimpleResponce{Code: 400, Message: l.ERRTokenRequested}
	}

	if input_.PhoneNumber == "" {
		log.Print(l.ERRPhNumRequested)
		return t.SimpleResponce{Code: 400, Message: l.ERRPhNumRequested}
	}

	//Поиск кошелка в бд и если не найден то возвращаем ошибку
	if result := db.Db.First(&dbWalletData, "token = ?", input_.Token); result.RowsAffected == 0 {
		log.Print(l.ERRNoSuchWalToken)
		return t.SimpleResponce{Code: 400, Message: l.ERRNoSuchWalToken}
	}

	//Поиск данных о телефоне в бд
	db.Db.First(&dbPhoneData, "phone_number = ?", input_.PhoneNumber)

	//Проверка на частый запрос кода
	if dbPhoneData.PhoneNumber != "" {
		if dbPhoneData.LastAttempt.Add(time.Minute).After(time.Now()) {
			log.Print(l.ERRTooFast)
			return t.SimpleResponce{Code: 400, Message: "Too fast!"}
		}
	}

	//Генерирование рандомного кода и его проверка
	randNumber, err := h.GetRandNum(4)
	if err != nil {
		return t.SimpleResponce{Code: 500, Message: err.Error()}
	}

	//Сообщение для проверки кода авторизации, можно удалить
	println("code is " + randNumber)

	//Генерирование хешсуммы для нашего сгенерированного кода
	hashRandNUmber := h.GenerateMD5(randNumber)

	//Отправка сообщения, создание лога и проверка на ошибку
	smsText := "Ваш код " + randNumber
	tempFrom := dbWalletData.Nickname
	if input_.From != "" {
		tempFrom = input_.From
	}
	smsResp, err := s.SendVSMS(&dbWalletData, input_.PhoneNumber, tempFrom, smsText)

	tempLog := t.SMSLog{WalletID: dbWalletData.ID, From: tempFrom, To: input_.PhoneNumber, Text: smsText}
	if smsResp != nil {
		tempLog.ServiceResponce = datatypes.JSON(smsResp)
	}
	db.Db.Create(&tempLog)

	if err != nil {
		return t.SimpleResponce{Code: 400, Message: err.Error()}
	}

	//Мягкое удаление всех остальных кодов с таким же телефоном
	db.Db.Where("phone_number = ?", input_.PhoneNumber).Delete(&t.VerificationCode{})

	//Созание новой связки номер-код в нашей бд
	db.Db.Create(&t.VerificationCode{ExpiredAt: time.Now().Add(time.Minute * 2), PhoneNumber: input_.PhoneNumber, AuthCode: hashRandNUmber})

	//Создание или сохранение данных о телефоне в нашей бд
	if dbPhoneData.PhoneNumber != "" {
		db.Db.Model(&t.RegisteredPhoneNumber{}).Where("phone_number = ?", input_.PhoneNumber).Update("last_attempt", time.Now())
	} else {
		dbPhoneData.LastAttempt = time.Now()
		dbPhoneData.PhoneNumber = input_.PhoneNumber
		db.Db.Create(&dbPhoneData)
	}

	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "CreateAndSendVerificationMessege")
	}

	return t.SimpleResponce{Code: 200, Message: "Success!"}
}

// Функция для проверки проверочного кода
func VerificateCode(input_ *t.InputVerify) t.SimpleResponce {
	if l.PrintInfo {
		log.Printf(l.INFFuncStart, "VerificateCode")
	}

	if input_ == nil {
		log.Print(l.ERRInputNilPointer)
		return t.SimpleResponce{Code: 500, Message: l.ERRInputNilPointer}
	}

	if input_.PhoneNumber == "" {
		log.Print(l.ERRPhNumRequested)
		return t.SimpleResponce{Code: 400, Message: l.ERRPhNumRequested}
	}

	if input_.Code == "" {
		log.Print(l.ERRCodeRequested)
		return t.SimpleResponce{Code: 400, Message: l.ERRCodeRequested}
	}

	//Переменная в которую будем помещать найденный телефон
	var dbPhoneData t.VerificationCode

	//Поиск наличие связки номер-код в нашей бд по нужному номеру телефона и проверка
	db.Db.First(&dbPhoneData, "phone_number = ?", input_.PhoneNumber)
	if dbPhoneData.PhoneNumber == "" {
		log.Print(l.ERRNoSuchPhNum)
		return t.SimpleResponce{Code: 400, Message: l.ERRNoSuchPhNum}
	}

	//Проверка на совпадение хэшсумм кодов
	if h.GenerateMD5(input_.Code) != dbPhoneData.AuthCode {
		log.Print(l.ERRWrongCode)
		return t.SimpleResponce{Code: 400, Message: l.ERRWrongCode}
	}

	//Проверка кода на его срок жизни
	if time.Now().After(dbPhoneData.ExpiredAt) {
		log.Print(l.ERRCodeExpired)
		return t.SimpleResponce{Code: 400, Message: l.ERRCodeExpired}
	}

	//Все проверки прошли успешно - мягко удаляем код и возвращаем успех
	db.Db.Where("phone_number = ?", input_.PhoneNumber).Delete(&t.VerificationCode{})

	if l.PrintInfo {
		log.Printf(l.INFFuncEnd, "VerificateCode")
	}

	return t.SimpleResponce{Code: 200, Message: "Success!"}

}

func SendTextSMS(input_ *t.InputSendSMS) (t.SimpleResponce, error) {

	log.Printf(l.INFFuncStart, "SendTextSMS")
	defer log.Printf(l.INFFuncEnd, "SendTextSMS")

	if input_ == nil {
		log.Print(l.ERRInputNilPointer)
		return t.SimpleResponce{Code: 500, Message: l.ERRInputNilPointer}, errors.New(l.ERRInputNilPointer)
	}

	if input_.PhoneNumber == "" {
		log.Print(l.ERRPhNumRequested)
		return t.SimpleResponce{Code: 400, Message: l.ERRPhNumRequested}, errors.New(l.ERRPhNumRequested)
	}

	//Проверка запроса на наличие поля token в теле
	if input_.Token == "" {
		log.Print(l.ERRTokenRequested)
		return t.SimpleResponce{Code: 400, Message: l.ERRTokenRequested}, errors.New(l.ERRTokenRequested)
	}
	//Проверка запроса на наличие поля text в теле
	if input_.Text == "" {
		log.Print(l.ERRTextRequested)
		return t.SimpleResponce{Code: 400, Message: l.ERRTextRequested}, errors.New(l.ERRTextRequested)
	}

	//Переменная в которую будем помещать найденный кошелек
	var dbWalletData t.Wallet

	//Поиск кошелка в бд и если не найден то возвращаем ошибку
	if result := db.Db.First(&dbWalletData, "token = ?", input_.Token); result.RowsAffected == 0 {
		log.Print(l.ERRNoSuchWalToken)
		return t.SimpleResponce{Code: 400, Message: l.ERRNoSuchWalToken}, errors.New(l.ERRNoSuchWalToken)
	}
	tempFrom := dbWalletData.Nickname
	if input_.From != "" {
		tempFrom = input_.From
	}
	//Отправка сообщения, создание лога и проверка на ошибку
	smsResp, err := s.SendVSMS(&dbWalletData, input_.PhoneNumber, tempFrom, input_.Text)

	tempLog := t.SMSLog{WalletID: dbWalletData.ID, From: input_.From, To: input_.PhoneNumber, Text: input_.Text}
	if smsResp != nil {
		tempLog.ServiceResponce = datatypes.JSON(smsResp)
	}
	db.Db.Create(&tempLog)

	if err != nil {
		log.Printf(l.ERR, err.Error())
		return t.SimpleResponce{Code: 400, Message: err.Error()}, err
	}

	return t.SimpleResponce{Code: 200, Message: "Success!"}, nil
}

func GetSMSLogs(input_ *t.InputFieldsLogSearch) (int, *t.SMSLogResponce, error) {

	if input_ == nil {
		log.Print(l.ERRInputNilPointer)
		return 500, nil, errors.New(l.ERRInputNilPointer)
	}

	//Проверка запроса на наличие поля nickname в теле
	if input_.Token == "" {
		log.Print(l.ERRTokenRequested)
		return 400, nil, errors.New(l.ERRTokenRequested)
	}

	//Переменная в которую будем помещать найденный кошелек
	var dbWalletData t.Wallet

	if result := db.Db.First(&dbWalletData, "token = ?", input_.Token); result.RowsAffected == 0 {
		log.Printf(l.ERRNoSuchWalToken)
		return 400, nil, errors.New(l.ERRNoSuchWalToken)
	}

	var logs []t.SMSLog
	tx := db.Db.Model(&t.SMSLog{}).Where("wallet_id = ?", dbWalletData.ID)
	if input_.Start != "" && input_.End != "" {
		tx = tx.Where("created_at BETWEEN ? and ?", input_.Start, input_.End)
	}
	if input_.Search != "" {
		input_.Search = "%" + input_.Search + "%"
		tx = tx.Where("\"to\" LIKE ? OR text LIKE ? OR \"from\" LIKE ?", input_.Search, input_.Search, input_.Search)
	} else {
		if input_.From != "" {
			tx = tx.Where("\"from\" = ?", input_.From)
		}
		if input_.To != "" {
			tx = tx.Where("\"to\" = ?", input_.To)
		}
		if input_.Text != "" {
			tx = tx.Where("text = ?", input_.Text)
		}
	}
	if input_.SortBy != "" {
		tx = tx.Order(input_.SortBy)
	}

	tx = tx.Find(&logs)

	tempLen := len(logs)

	tx = db.Paginate(&input_.Page, &input_.PageSize)(tx)
	tx = tx.Find(&logs)

	fmt.Printf("%v", logs)

	return 200, &t.SMSLogResponce{Items: logs, TotalItems: tempLen, TotalPages: int(math.Ceil(float64(tempLen) / float64(h.Max(1, input_.PageSize))))}, nil
}
