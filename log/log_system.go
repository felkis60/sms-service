package log

import (
	"log"
	"os"
)

func StartLogs() {

	if os.Getenv("LOG_FILE") == "true" {
		file, err := os.OpenFile("logs.txt", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.Fatal(err)
		}
		log.SetOutput(file)
	}
	log.SetFlags(log.LstdFlags | log.Lshortfile)
	if os.Getenv("INFOLOG") == "true" {
		PrintInfo = true
	}
}

var (
	ERR                = "ERROR: %v"
	ERREnvLoad         = "ERROR: Failed to load .env file"
	ERRDbConnect       = "ERROR: Failed to connect database: %v"
	ERRAutoMigrate     = "ERROR: Failed to automigrate: %v"
	ERRListen          = "ERROR: Failed to listen: %v"
	ERRServe           = "ERROR: Failed to serve: %v"
	ERRTextRequested   = "ERROR: 'text' is required"
	ERRTokenRequested  = "ERROR: 'token' is required"
	ERRPhNumRequested  = "ERROR: 'phone_number' is required"
	ERRCodeRequested   = "ERROR: 'code' is required"
	ERRInputNilPointer = "ERROR: invalid request"
	ERRWrongCode       = "ERROR: Wrong code"
	ERRNoSuchPhNum     = "ERROR: No such phone number"
	ERRNoSuchWalToken  = "ERROR: No such wallet token"
	ERRTooFast         = "ERROR: Too fast"
	ERRCodeExpired     = "ERROR: Code is expired"
	INF                = "INFO: %v"
	INFReceived        = "INFO: Received: %v"
	INFStartServer     = "INFO: %v server starts"
	INFFuncStart       = "INFO: Function %v start"
	INFFuncEnd         = "INFO: Function %v end"
)

var (
	PrintInfo = false
)
