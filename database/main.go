package database

import (
	"log"
	"os"

	l "gitlab.com/felkis60/sms-service/log"
	t "gitlab.com/felkis60/sms-service/models"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB

func Start() {

	dsn := "host=" + os.Getenv("DB_HOST") +
		" user=" + os.Getenv("DB_USER") +
		" password=" + os.Getenv("DB_PASSWORD") +
		" dbname=" + os.Getenv("DB_NAME") +
		" port=" + os.Getenv("DB_PORT") +
		" sslmode=" + os.Getenv("DB_SSL_MODE") +
		" TimeZone=" + os.Getenv("DB_TIME_ZONE")

	var err error
	Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatalf(l.ERRDbConnect, err.Error())
	}
}

func Migrate() {
	err := Db.AutoMigrate(&t.VerificationCode{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.RegisteredPhoneNumber{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.Wallet{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}

	err = Db.AutoMigrate(&t.SMSLog{})
	if err != nil {
		log.Fatalf(l.ERRAutoMigrate, err.Error())
	}
}

func Paginate(page_ *int, pageSize_ *int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {

		if *page_ <= 0 {
			*page_ = 1
		}

		switch {
		case *pageSize_ > 100:
			*pageSize_ = 100
		case *pageSize_ < 5:
			*pageSize_ = 5
		}

		offset := (*page_ - 1) * *pageSize_
		return db.Offset(offset).Limit(*pageSize_)
	}
}
