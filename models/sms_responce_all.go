package models

//Структура для ответа смс сервиса
type SMSResponceAll struct {
	MessageCount string                `json:"message-count"`
	Messages     []SMSResponceMessages `json:"messages"`
}
