package models

import "time"

//Структура для таблицы данных о номере
type RegisteredPhoneNumber struct {
	PhoneNumber string `gorm:"index"`
	LastAttempt time.Time
}
