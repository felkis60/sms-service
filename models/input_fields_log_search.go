package models

type InputFieldsLogSearch struct {
	Token    string `form:"token" json:"token" example:"dh3ufno348jpowe94inios8e92j0j39jf"`
	PageSize int    `form:"page_size" json:"page_size"`
	Page     int    `form:"page" json:"page"`
	Search   string `form:"search" json:"search" example:"pets"`
	From     string `form:"from" json:"from" example:"REX/79999999999"`
	To       string `form:"to" json:"to" example:"79999999999"`
	Text     string `form:"text" json:"text" example:"code is"`
	Start    string `form:"start" json:"start" example:"date start"`
	End      string `form:"end" json:"end" example:"date end"`
	SortBy   string `form:"sort_by" json:"sort_by" example:"from/to/text/created_at"`
}
