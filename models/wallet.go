package models

//Структура для таблицы с данными о кошелке для смс сервиса
type Wallet struct {
	ID        uint   `gorm:"primarykey"`
	Nickname  string `gorm:"index"`
	Key       string
	Secret    string
	Token     string `gorm:"index size:64"`
	SMSCLogin string `json:"smsc_login"`
	SMSCPass  string `json:"smsc_pass"`
}
