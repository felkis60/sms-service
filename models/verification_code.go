package models

import (
	"time"

	"gorm.io/gorm"
)

//Структура для таблицы связки номер-код
type VerificationCode struct {
	CreatedAt   time.Time
	ExpiredAt   time.Time
	DeletedAt   gorm.DeletedAt `gorm:"index"`
	PhoneNumber string         `gorm:"index"`
	AuthCode    string
}
