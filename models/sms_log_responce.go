package models

type SMSLogResponce struct {
	Items      []SMSLog `json:"items"`
	TotalItems int      `json:"total_items"`
	TotalPages int      `json:"total_pages"`
}
