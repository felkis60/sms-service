package models

//Структура для тела запроса
type InputVerify struct {
	PhoneNumber string `form:"phone_number" json:"phone_number" example:"79999999999"`
	Code        string `form:"code" json:"code" example:"8630"`
}
