package models

//Структура для тела запроса
type InputRegistration struct {
	Token       string `form:"token" json:"token" example:"dh3ufno348jpowe94inios8e92j0j39jf"`
	PhoneNumber string `form:"phone_number" json:"phone_number" example:"79999999999"`
	From        string `form:"from" json:"from" example:"REX/79999999999"`
}
