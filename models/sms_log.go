package models

import (
	"time"

	"gorm.io/datatypes"
)

//Структура для таблицы смс логирования
type SMSLog struct {
	CreatedAt       time.Time
	WalletID        uint           `gorm:"index" json:"wallet_id"`
	From            string         `json:"from"`
	To              string         `gorm:"index" json:"to"`
	Text            string         `json:"text"`
	ServiceResponce datatypes.JSON `json:"service_responce"`
}
