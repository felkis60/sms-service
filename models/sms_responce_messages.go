package models

//Под-структура для ответа смс сервиса
type SMSResponceMessages struct {
	To               string `json:"to"`
	Status           string `json:"status"`
	ErrorText        string `json:"error-text"`
	MessageId        string `json:"message-id"`
	RemainingBalance string `json:"remaining-balance"`
	MessagePrice     string `json:"message-price"`
	Network          string `json:"network"`
}

type SMSCResponce struct {
	ID        *string  `json:"id"`
	Cost      *string  `json:"cost"`
	Cnt       *string  `json:"cnt"`
	Balance   *string  `json:"balance"`
	Error     *string  `json:"error"`
	ErrorCode *float64 `json:"error_code"`
}
