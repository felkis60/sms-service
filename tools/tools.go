package tools

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/hex"
	"errors"
	"io"
)

//Функция для создания случайного x-значного числа
func GetRandNum(digitAmount_ int) (string, error) {

	if digitAmount_ <= 0 {
		return "", errors.New("getRandNum: digitAmount_ must be greater than zero!")
	}

	b := make([]byte, digitAmount_)
	n, err := io.ReadAtLeast(rand.Reader, b, digitAmount_)
	if n != digitAmount_ {
		return "", err
	}
	for i := 0; i < len(b); i++ {
		b[i] = digitTable[int(b[i])%len(digitTable)]
	}

	return string(b), nil
}

var digitTable = [...]byte{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'}

//Функция генерации MD5 хэша из строки
func GenerateMD5(str string) string {
	hash := md5.Sum([]byte(str))
	md5str := hex.EncodeToString(hash[:])
	return md5str
}

// Max returns the larger of x or y.
func Max(x, y int) int {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func Min(x, y int) int {
	if x > y {
		return y
	}
	return x
}
